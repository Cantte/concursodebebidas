/**
 * @autor Carlos Pavajeau
 * @version 15.05.2019
 */

 package Vista;

 import java.util.Scanner;

import Modelo.*;
 
 public class Main 
 {
     private static Bebida[] bebidas = new Bebida[4];
     private static Concursante[] concursantes = new Concursante[3];
     private static Scanner input = new Scanner(System.in);
     private static String[] opciones = {"A.Registrar concursantes", "B.Iniciar concurso", "C.Ganador del concurso", "D.Salir"};
     public static void main(String[] args) 
     {
        char op = '\0';

        CrearBebidas();

        while ((op = Menu()) != 'D')
        {
            switch (op)
            {
                case 'A':
                {
                    RegistrarConcursantes();
                    break;
                }
                case 'B':
                {
                    IniciarJuego();
                    break;
                }
                case 'C':
                {
                    ImprimirGanador();
                    break;
                }
                case 'D':
                {
                    break;
                }
            }
        }
     }

     public static void RegistrarConcursantes()
     {
         System.out.println("Registro de los concursantes");
         for (int i = 0; i < 3; i++)
         {
            concursantes[i] = RegistrarConcursante();
         }
     }

     public static Concursante RegistrarConcursante()
     {
        System.out.println("Registro de concursante");
        System.out.printf("Digite el nombre: ");
        String nombre = input.next();
        System.out.println("Digite el tipo de bebida. Los tipos de bebida son: ");
        for (int i = 0; i < 4; i++)
        {
            System.out.println("" + (i + 1) + ". " + bebidas[i].GetTipo() + " de " + bebidas[i].GetContenido() + "cm3 con inscripcion de " + bebidas[i].GetPrecio());
        }
        System.out.printf("\nTipo de bebida: ");
        int bebida = input.nextInt();
        while (bebida < 0 || bebida > 4)
        {
            System.out.println("Tipo de bebida incorrecta, digite una válida");
            System.out.printf("Tipo de bebida: ");
            bebida = input.nextInt();
        }

        return new Concursante(nombre, new Bebida(bebidas[bebida - 1]));
     }

     public static void IniciarJuego()
     {
         for (Concursante crst : concursantes)
            if (crst == null)
            {
                System.out.println("Concursantes no registrados, no se puede inicar el juego");
                return;
            }
        do
        {
            for (Concursante crst : concursantes)
                crst.TomarUnTrago(crst.CantidadATomar());
            
            for (Concursante crst : concursantes)
            {
                System.out.printf("%s tomo un sorbo de %dcc y le quedan %dcc%n", crst.GetNombre(), crst.GetCantidadTomada(), crst.GetBebida().GetContenido());
            }

        } while (!concursantes[0].BebidaTerminada() && !concursantes[1].BebidaTerminada() && !concursantes[2].BebidaTerminada());
     }

     public static void ImprimirGanador()
     {
        for (Concursante crst : concursantes)
        if (crst == null)
        {
            System.out.println("Concursantes no registrados, no se puede determinar un ganador");
            return;
        }

        for (Concursante crst : concursantes)
        {
            if (crst.BebidaTerminada())
            {
                System.out.printf("¡Felicidades! %s ha ganado luego de %d tragos. Su permio es de: %.2f%n", crst.GetNombre(), crst.GetTragos(), crst.GetBebida().GetPrecio() * 1000);
                break;
            }
        }

     }

     public static char Menu()
     {
        char op = 0;
        for (String ops : opciones)
            System.out.println(ops);

        System.out.printf("Digite una opción: ");    
        op = input.next().toUpperCase().charAt(0);

        while (!OpcionCorrecta(op))
        {
            System.out.println("Opción incorrecta, por favor digite una opción correcta");
            System.out.printf("Digite una opción: ");    
            op = input.next().toUpperCase().charAt(0);
        }

        return op;
     }

     public static boolean OpcionCorrecta(char op)
     {
         return op == 'A' || op == 'B' || op == 'C' || op == 'D';
     }

     public static void CrearBebidas()
     {
        bebidas[0] = new Bebida("Lata", 350, 350);
        bebidas[1] = new Bebida("Botella", 1000, 500);
        bebidas[2] = new Bebida("Botella", 2000, 800);
        bebidas[3] = new Bebida("Botella", 3000, 1000);
     }
 }