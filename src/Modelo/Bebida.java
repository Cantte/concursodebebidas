/*
 * @autor Carlos Pavajeau
 * @version 15.05.2019
*/ 

package Modelo;

public class Bebida
{
    private String m_tipo;
    private int m_cm3;
    private float m_precio;

    public Bebida(String tipo, int cm3, float precio)
    {
        m_tipo = tipo;
        m_cm3 = cm3;
        m_precio = precio;
    }

    public Bebida(Bebida bebida)
    {
        m_tipo = bebida.m_tipo;
        m_cm3 = bebida.m_cm3;
        m_precio = bebida.m_precio;
    }

    public Bebida()
    {
        this("", 0, 0.0f);
    }

    public String GetTipo()
    {
        return m_tipo;
    }

    public int GetContenido()
    {
        return m_cm3;
    }

    public float GetPrecio()
    {
        return m_precio;
    }

    public void Tomar(int cm3)
    {
        if (cm3 >= m_cm3)
        {
            m_cm3 = 0;
            return;
        }
        if (cm3 > 0 && m_cm3 > 0)
            m_cm3 -= cm3;
    }

    public boolean Vacia()
    {
        return m_cm3 == 0;
    }
}