/**
 * @autor Carlos Pavajeau
 * @version 15.05.2019
 */

 package Modelo;

import java.security.SecureRandom;

public class Concursante 
 {
     private String m_nombre;
     private Bebida m_bebida;
     private int m_tragos;
     private int m_cantidadATomar;
     public Concursante(String nombre, Bebida bebida)
     {
         m_nombre = nombre;
         m_bebida = bebida;
         m_tragos = 0;
         m_cantidadATomar = 0;
     }

     public String GetNombre()
     {
         return m_nombre;
     }

     public Bebida GetBebida()
     {
         return m_bebida;
     }

     public int GetTragos()
     {
         return m_tragos;
     }

     public int GetCantidadTomada()
     {
        return m_cantidadATomar;
     }

     public void SetNombre(String nombre)
     {
         m_nombre = nombre;
     }

     public void SetBebida(Bebida bebida)
     {
        m_bebida = bebida;
     }

     public void SetTragos(int tragos)
     {
         m_tragos = tragos;
     }

     public void SetCantidadTomada(int cantidad)
     {
         m_cantidadATomar = cantidad;
     }

     public int CantidadATomar()
     {
         SecureRandom random = new SecureRandom();
         m_cantidadATomar = 10 + random.nextInt(90);
         return m_cantidadATomar;
     }

     public int TomarUnTrago(int trago)
     {
         m_bebida.Tomar(trago);
         m_tragos++;
         return m_bebida.GetContenido();
     }

     public boolean BebidaTerminada()
     {
         return m_bebida.Vacia();
     }
 }